﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace metodo_de_horner
{
    class Program
    {
        static void Main(string[] args)
        {
            
            int Decimal = 0;
            Ecuacion e = new Ecuacion();


            Console.WriteLine("ingrese los elementos que tendra la ecuacion: ");
            e.Iniciar(Convert.ToInt32(Console.ReadLine()));

            for (int i = 0; i < e.Funcion.Length; i++)
            {

                Console.WriteLine("Constante o variable?\nEscribe C o V");

                String primeraFase = Console.ReadLine();
                if (primeraFase.Equals("C") || primeraFase.Equals("c"))
                {
                    Console.WriteLine("La constante es positiva o negativa?\nEscribe P o N");

                    String constanteFaseUno = Console.ReadLine();

                    bool flagUno = true;
                    bool operacion = false;
                    do
                    {
                        if (constanteFaseUno.Equals("P") || constanteFaseUno.Equals("p"))
                        {
                            operacion = true;
                        }
                        else if (constanteFaseUno.Equals("N") || constanteFaseUno.Equals("n"))
                        {
                            operacion = false;
                        }
                        else
                        {
                            Console.WriteLine("No puedes colocar un digito o caracter distinto");

                            constanteFaseUno = "";
                            flagUno = false;
                        }
                    } while (flagUno != true);


                    Console.WriteLine("Ingrese el numero, solo valores positivos:");

                    String constanteFaseDos = Console.ReadLine();


                    e.Funcion[i] = 1;

                    if (operacion)
                    {
                        e.Producto[i] = Double.Parse(constanteFaseDos);
                    }
                    else
                    {
                        String aux2 = "-";

                        String aux = aux2 + constanteFaseDos;


                        e.Producto[i] = Double.Parse(aux);


                    }

                    e.Potencia[i] = 1;


                }
                else if (primeraFase.Equals("V") || primeraFase.Equals("v"))
                {


                    Console.WriteLine("Tiene alguna constante para multiplicar por la variable?");

                    Double variableFaseDos = Convert.ToDouble(Console.ReadLine());

                    Console.WriteLine("La Variable es positiva o negativa?\nEscribe P o N");


                    String variableFaseUno = Console.ReadLine();
                    bool flagUno = true;
                    bool operacion = false;

                    do
                    {
                        if (variableFaseUno.Equals("P") || variableFaseUno.Equals("p"))
                        {
                            operacion = true;
                        }
                        else if (variableFaseUno.Equals("N") || variableFaseUno.Equals("n"))
                        {
                            operacion = false;
                        }
                        else
                        {
                            Console.WriteLine("No puedes colocar un digito o caracter distinto");

                            variableFaseUno = "";
                            flagUno = false;
                        }
                    } while (flagUno != true);
                    Console.WriteLine("Ingrese el exponente de la variable:");
                    Double variableFaseTres = Convert.ToDouble(Console.ReadLine());
                    e.Potencia[i] = variableFaseTres;


                    if (operacion)
                    {
                        e.Producto[i] = variableFaseDos;
                    }
                    else
                    {
                        String aux2 = "-";

                        String aux = aux2 + variableFaseDos;

                        variableFaseDos = Double.Parse(aux);

                        e.Producto[i] = variableFaseDos;
                    }

                    e.Funcion[i] = 100;

                }
                else
                {
                    Console.WriteLine("No puedes colocar un digito o caracter distinto");
                    primeraFase = "";
                    i--;
                }


            }



            //hasta este punto ya tenemos la funcion
            //lo siguiente es mostrarla en pantalla

            Console.WriteLine("esta ha sido la funcion");
            Console.WriteLine("" + e.mostrarFuncion() + "\n");
            e.funcionPolinomica();




            //hasta aqui pedimos si el usuario quiere introducir los invervalos validos, o que el sistema los encuentre.
            Console.WriteLine("\nPresione 0 para introducir los intervalos.\nPresione 1 para que el sistema encuentre los intervalos.\nIngrese opcion: ");
            String op = Console.ReadLine();

            double intervaloA = 0;
            double intervaloB = 0;
            double resultadoA = 0;
            double resultadoB = 0;
            bool tem = false;
            bool flag = false;
            double puntoMedio = 0;

            //aqui hacemos el switch que nos determinara los intervalos
            do
            {


                switch (op)
                {
                    case "0":
                        do
                        {

                            Console.WriteLine("ingrese intervalo A: ");
                            intervaloA = Convert.ToDouble(Console.ReadLine());
                            Console.WriteLine("ingrese intervalo B: ");
                            intervaloB = Convert.ToDouble(Console.ReadLine());

                            tem = e.intervaloUsuario(intervaloA, intervaloB);
                            if (tem)
                            {
                                Console.WriteLine("El resultado es bueno y podemos iterar sobre estos valores\n");
                                op = "1";
                                puntoMedio = (intervaloA + intervaloB) / 2;
                            }
                            else
                            {
                                Console.WriteLine("El resultado es malo, no se pueden trabajar estos valores");
                                Console.WriteLine("Desea seguir escogiendo introducir los datos?" +
                                        "\nPresione 0 para introducir nuevos intervalos\n" +
                                        "Presione 1 para dejar que el programa busque los intervalo\n" +
                                        "Escoja una opcion: ");
                                op = Console.ReadLine();
                                if (op.Equals("1"))
                                {
                                    tem = true;
                                }
                            }


                        } while (op != "1");

                        tem = false;
                        break;
                    case "1":
                        Console.WriteLine("LLegamos con bien hasta aqui");

                        bool res = e.intervaloAutomatico();
                        if (res)
                        {
                            Console.WriteLine("Se encontraron los intervalos para iterar:\n" +
                                    "Intervalo A: " + e.iA + "" +
                                    "\nIntervalo B: " + e.iB);

                            puntoMedio = (e.iA + e.iB) / 2;
                            tem = true;
                        }
                        else
                        {
                            Console.WriteLine("muy probablemente la funcion sea con raices imaginarias :3");


                        }
                        break;

                }

            } while (tem != true);

            double Xn = 0;
            double Xa = 0;
            double error = 0.05;
            double errorRelativo = 100;
            double x0 = puntoMedio;
            double Iteracion = 0;
            int item = 1;

            //hacemos el procedimiento de horner

            do
            {


                Iteracion = x0 - (e.HornerR(x0) / e.HornerAsterisco(x0));

                Iteracion = e.redondeo(Iteracion, 4);
                Xn = Iteracion;
                Xa = x0;

                Console.WriteLine("-----------------------------------------------Iteracion " + item + "\nla raiz vale : " + Iteracion);
                errorRelativo = Math.Abs((Xn - Xa) / Xn) * 100;
                Console.WriteLine(Xn + " - " + Xa + "/" + Xn);
                errorRelativo = e.redondeo(errorRelativo, 5);
                Console.WriteLine("el error relativo es : " + errorRelativo + "%\n");

                x0 = Iteracion;
                item++;
            } while (errorRelativo > error);

            Console.WriteLine("\nla raiz es : " + Iteracion);
        }
    }
 }

