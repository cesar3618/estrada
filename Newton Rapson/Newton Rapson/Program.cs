﻿using metodo_de_horner;
using System;

namespace Newton_Rapson
{
    class Program
    {
        double auxIteracion = 100;
        double aux2Iteracion = 0;
        double aux2IteracionPrima = 0;
        double auxIteracionPrima = 100;
        int Decimales;


        bool[] validar;
        bool[] validarPrima;

        int Contador = 0;
        int ContadorPrima = 0;
        static void Main(string[] args)
        {
            int Decimal = 0;
            Ecuacion e = new Ecuacion();


            Console.WriteLine("ingrese los elementos que tendra la ecuacion: ");
            e.Iniciar(Convert.ToInt32(Console.ReadLine()));

            for (int i = 0; i < e.Funcion.Length; i++)
            {

                Console.WriteLine("Constante o variable?\nEscribe C o V");

                String primeraFase = Console.ReadLine();
                if (primeraFase.Equals("C") || primeraFase.Equals("c"))
                {
                    Console.WriteLine("La constante es positiva o negativa?\nEscribe P o N");

                    String constanteFaseUno = Console.ReadLine();

                    bool flagUno = true;
                    bool operacion = false;
                    do
                    {
                        if (constanteFaseUno.Equals("P") || constanteFaseUno.Equals("p"))
                        {
                            operacion = true;
                        }
                        else if (constanteFaseUno.Equals("N") || constanteFaseUno.Equals("n"))
                        {
                            operacion = false;
                        }
                        else
                        {
                            Console.WriteLine("No puedes colocar un digito o caracter distinto");

                            constanteFaseUno = "";
                            flagUno = false;
                        }
                    } while (flagUno != true);


                    Console.WriteLine("Ingrese el numero, solo valores positivos:");

                    String constanteFaseDos = Console.ReadLine();

                    
                    e.Funcion[i] = Convert.ToDouble(constanteFaseDos);

                    if (operacion)
                    {
                        e.Producto[i] = 1;
                    }
                    else
                    {
                        String aux2 = "-";

                        String aux = aux2 + 1;


                        e.Producto[i] = Convert.ToDouble(aux);


                    }

                    e.Potencia[i] = 1;


                }
                else if (primeraFase.Equals("V") || primeraFase.Equals("v"))
                {


                    Console.WriteLine("Tiene alguna constante para multiplicar por la variable?");

                    double variableFaseDos = Convert.ToDouble(Console.ReadLine());

                    Console.WriteLine("La Variable es positiva o negativa?\nEscribe P o N");


                    String variableFaseUno = Console.ReadLine();
                    bool flagUno = true;
                    bool operacion = false;

                    do
                    {
                        if (variableFaseUno.Equals("P") || variableFaseUno.Equals("p"))
                        {
                            operacion = true;
                        }
                        else if (variableFaseUno.Equals("N") || variableFaseUno.Equals("n"))
                        {
                            operacion = false;
                        }
                        else
                        {
                            Console.WriteLine("No puedes colocar un digito o caracter distinto");

                            variableFaseUno = "";
                            flagUno = false;
                        }
                    } while (flagUno != true);
                    Console.WriteLine("Ingrese el exponente de la variable:");
                    double variableFaseTres = Convert.ToDouble(Console.ReadLine());
                    e.Potencia[i] = variableFaseTres;


                    if (operacion)
                    {
                        e.Producto[i] = variableFaseDos;
                    }
                    else
                    {
                        String aux2 = "-";

                        String aux = aux2 + variableFaseDos;

                        variableFaseDos = Convert.ToDouble (aux);

                        e.Producto[i] = variableFaseDos;
                    }

                    e.Funcion[i] = 100;

                }
                else
                {
                    Console.WriteLine("No puedes colocar un digito o caracter distinto");
                    primeraFase = "";
                    i--;
                }


            }


            //hasta este punto ya tenemos la funcion
            //lo siguiente es mostrarla en pantalla

            Console.WriteLine("esta ha sido la funcion");
            Console.WriteLine("" + e.mostrarFuncion(1));
            e.derivada();
            Console.WriteLine("este es la derivada");
            Console.WriteLine("" + e.mostrarFuncion(2));


            Program p = new Program();

            p.setBool(e.Funcion.Length);

            //numero de decimales a trabajar
            Console.WriteLine("Dame el numero de decimales a trabajar: ");

            String decimales = Console.ReadLine();
            try
            {
                Decimal = Convert.ToInt32(decimales);
                p.setDecimal(Decimal);
            }
            catch (Exception w)
            {

            }

            //lo siguiente es pedir el intervalo de valores donde se analizara la funcion.

            Console.WriteLine("Ingrese el intervalo: ");

            String intervalo = Console.ReadLine();

            double ResIntervalo = p.Iteracion(Convert.ToDouble(intervalo), e);
            Console.WriteLine(ResIntervalo);
            Console.WriteLine();

            double eme = p.IteracionDerivada(Convert.ToDouble(intervalo), e);


            Console.WriteLine(eme);
            //pedimos porcentaje de error
            Console.WriteLine("Dame el portentaje de error: ");

            String portError = Console.ReadLine();

            double Error = 0;
            try
            {
                Error = Convert.ToDouble(portError);
            }
            catch (Exception w)
            {

            }

            //Aqui hacemos la integracion del metodo de Netwon

            double xr = Convert.ToDouble(intervalo);
            double xrR;
            double error = 100;
            double item = 1;

            do
            {

                xrR = xr - (p.Iteracion(xr, e) / p.IteracionDerivada(xr, e));
                xrR = p.setRedondeo(xrR, Decimal);


                error = Math.Abs(((xrR - xr) / xrR)) * 100;
                Console.WriteLine("es el error absoluto " + error + "%");
                xr = xrR;

                Console.WriteLine("este es el valor de x " + xrR);
                item++;

            } while (error > Error || item < 50);

            Console.WriteLine("es el valor final " + xr);


        }
        //fuera del main

        public double Iteracion(double a, Ecuacion e)
        {
            for (int i = 0; i < e.Funcion.Length; i++)
            {
                if (Contador == 0)
                {
                    if (e.Funcion[i] == auxIteracion)
                    {
                        e.Funcion[i] = a;
                        validar[i] = true;
                    }
                }
                else
                {
                    if (e.Funcion[i] == auxIteracion && validar[i] == true)
                    {
                        e.Funcion[i] = a;
                    }
                }
            }
            auxIteracion = a;
            double[] funcionFalsa = new double[e.Funcion.Length];
            for (int i = 0; i < e.Funcion.Length; i++)
            {
                aux2Iteracion = Math.Pow(e.Funcion[i], e.Potencia[i]);
                aux2Iteracion = aux2Iteracion * e.Producto[i];
                funcionFalsa[i] = aux2Iteracion;
            }
            double aux4 = 0;
            double aux5 = 0;
            for (int i = 0; i < e.Funcion.Length; i++)
            {
                aux4 = funcionFalsa[i];
                aux5 = aux5 + aux4;
            }
            Contador++;
            return setRedondeo(aux5, Decimales);
        }


        public double IteracionDerivada(double a, Ecuacion e)
        {
            for (int i = 0; i < e.FuncionPrima.Length; i++)
            {
                if (ContadorPrima == 0)
                {
                    if (e.FuncionPrima[i] == auxIteracionPrima)
                    {
                        e.FuncionPrima[i] = a;
                        validarPrima[i] = true;
                    }
                }
                else
                {
                    if (e.FuncionPrima[i] == auxIteracionPrima && validarPrima[i] == true)
                    {
                        e.FuncionPrima[i] = a;
                    }
                }
            }
            auxIteracionPrima = a;
            double[] funcionFalsaPrima = new double[e.FuncionPrima.Length];
            for (int i = 0; i < e.FuncionPrima.Length; i++)
            {
                aux2IteracionPrima = Math.Pow(e.FuncionPrima[i], e.PotenciaPrima[i]);
                aux2IteracionPrima = aux2IteracionPrima * e.ProductoPrima[i];
                funcionFalsaPrima[i] = aux2IteracionPrima;
            }
            double aux4 = 0;
            double aux5 = 0;
            for (int i = 0; i < e.FuncionPrima.Length; i++)
            {
                aux4 = funcionFalsaPrima[i];
                aux5 = aux5 + aux4;
            }
            ContadorPrima++;
            return setRedondeo(aux5, Decimales);
        }



        //funcion para redondear resultados
        public double setRedondeo(double a, int b)
        {
            CRedondeo c = new CRedondeo();
            return c.redondeoExacto(a, b);
        }


        public void setDecimal(int a)
        {
            this.Decimales = a;
        }


        public void setBool(int a)
        {
            validar = new bool[a];
            validarPrima = new bool[a];
        }

        public int getDecimal()
        {
            return Decimales;
        }
    }

}